﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyCollision : MonoBehaviour {

    [Tooltip("FX prefab on player")] [SerializeField] GameObject deathFX; 
    [SerializeField] int scorePerHit = 12;
    [SerializeField] int hits = 3;

    private bool isDead = false;
    ScoreBoard scoreBoard;

    private void Start()
    {
        scoreBoard = FindObjectOfType<ScoreBoard>();
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.name == "Tank")
        {
            KillTheObject();
        }
    }

    private void OnParticleCollision(GameObject other)
    {
        if (!isDead)
        {
            ProcessHit();
        }
        if (hits <= 0)
        {
            KillTheObject();
        }
        //Destroy(gameObject,1f);   
    }
    private void KillTheObject()
    {
        StartDeathSequence();
        deathFX.SetActive(true);
        isDead = true;
    }
    private void StartDeathSequence()
    {
        SendMessage("OnEnemyDeath");
    }

    private void ProcessHit()
    {
        scoreBoard.ScoreHit(scorePerHit);
        hits--;
    }



}
