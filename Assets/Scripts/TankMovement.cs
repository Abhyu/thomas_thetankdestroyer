﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityStandardAssets.CrossPlatformInput;

[RequireComponent(typeof(Rigidbody))]
public class TankMovement : MonoBehaviour {

    [Header("General")]
    [SerializeField]
    private float tankSpeed = 12f;
    [SerializeField]
    private float turnSpeed = 180f;
    [SerializeField]
    private float pitchRange = 0.2f;
    [SerializeField]
    private GameObject bullets;

    [Header("Sound")]
    [SerializeField]
    private AudioSource movementAudio;
    [SerializeField]
    private AudioClip EngineIdle;
    [SerializeField]
    private AudioClip EngineDriving;

    private Rigidbody rb;
    private float _xMov;
    private float _zMov;
    private float originalPitch;
    private Vector3 velocity = Vector3.zero;
    private Vector3 rotation = Vector3.zero;
    private bool isControlledEnabled = true;

    // Use this for initialization
    void Start () {
        rb = GetComponent<Rigidbody>();
        originalPitch = movementAudio.pitch;
    }

    //private void OnEnable()
    //{
    //    rb.isKinematic = false;      
    //}


    //private void OnDisable()
    //{
    //    rb.isKinematic = true;
    //}

    // Update is called once per frame
    void Update () {
        if (isControlledEnabled)
        {
            //Calculate movement velocity as 3D vector
            _xMov = CrossPlatformInputManager.GetAxis("Horizontal");
            _zMov = CrossPlatformInputManager.GetAxis("Vertical");
            Vector3 _moveVertical = transform.forward * _zMov;

            //Final movement Vector
            Vector3 _velocity = (_moveVertical).normalized * tankSpeed;

            //Calculate turn speed
            float turn = _xMov * turnSpeed;
            Vector3 _rotation = new Vector3(0, turn, 0);

            //Apply movement
            Move(_velocity);
            //Apply rotation
            Rotate(_rotation);

            EngineAudio();
        }
    }

    public void Move(Vector3 _velocity)
    {
        velocity = _velocity;
    }

    private void Rotate(Vector3 _rotation)
    {
        rotation = _rotation;
    } 

    private void FixedUpdate()
    {
        if (isControlledEnabled)
        {
            PerformMovement();
            PerformRotate();
            PerformFiring();
        }
    }
    
    private void PerformMovement()
    {
        if (velocity != Vector3.zero)
        {
            rb.MovePosition(rb.position + velocity * Time.fixedDeltaTime);
        }
    }
     
    private void PerformRotate()
    {
        rb.MoveRotation(rb.rotation * Quaternion.Euler(rotation));
    }

    private void PerformFiring()
    {
        if (CrossPlatformInputManager.GetButton("Fire"))
        {
            ActivateBullets();
        }
        else
        {
            DeactivateBullets();
        }
    }
    
    private void EngineAudio()
    {
        //Play the correct audio clip based on whether or not the tank is moving and which audio is currently playing
        if (Mathf.Abs(_xMov)< 0.1f && Mathf.Abs(_zMov) < 0.1f )
        {
            if (movementAudio.clip == EngineDriving) 
            {
                movementAudio.clip = EngineIdle;
                movementAudio.pitch = UnityEngine.Random.Range(originalPitch - pitchRange, originalPitch+pitchRange);
                movementAudio.Play();
            }
        }
        else
        {
            if (movementAudio.clip == EngineIdle)
            {
                movementAudio.clip = EngineDriving;
                movementAudio.pitch = UnityEngine.Random.Range(originalPitch - pitchRange, originalPitch + pitchRange);
                movementAudio.Play();
            }
        }
    }

    void OnPlayerDeath() //called by string reference 
    {
        isControlledEnabled = false;
    }

    private void ActivateBullets()
    {
        bullets.SetActive(true);
    }

    private void DeactivateBullets()
    {
        bullets.SetActive(false);
    }
}
