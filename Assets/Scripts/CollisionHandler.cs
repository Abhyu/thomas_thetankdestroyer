﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class CollisionHandler : MonoBehaviour {

    [Tooltip("In seconds")] [SerializeField] float loadLevelDelay = 1f;
    [Tooltip("FX prefab on player")] [SerializeField] GameObject deathFX;

    [SerializeField] int healthPerHit = 10;
    [SerializeField] int hits = 10;
    HealthBoard healthBoard;

    private void Start()
    {
        healthBoard = FindObjectOfType<HealthBoard>();        
    }

    private void OnTriggerEnter(Collider other)
    {
        KillTheObject();
    }

    private void OnParticleCollision(GameObject other)
    {
        ProcessHit();
        if (hits<=0)
        {
            KillTheObject();
        }
    }

    private void KillTheObject()
    {
        StartDeathSequence();
        deathFX.SetActive(true);
    }

    private void StartDeathSequence()
    {
        SendMessage("OnPlayerDeath");
        Invoke("Reload", loadLevelDelay);
    }

    private void ProcessHit()
    {
        healthBoard.ReduceHealth(healthPerHit);
        hits--;
    }

    private void Reload() //string reference
    {
        SceneManager.LoadScene(1);
    }
}
