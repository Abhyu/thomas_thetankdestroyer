﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyController : MonoBehaviour {

    [SerializeField]
    private Transform objectToPan;
    [SerializeField]
    private GameObject bullets;
    [SerializeField]
    private float speed = 0.05f;
    private bool isControlledEnabled = true;
    private Transform player;

    // Use this for initialization
    void Start () {
		player = GameObject.FindWithTag("Player").transform;
    }
	
	// Update is called once per frame
	void Update ()
    {
        if (isControlledEnabled)
        {
            LookAndMove();
        }
    }

    private void LookAndMove()
    {
        Vector3 direction = player.position - this.transform.position;
        this.transform.rotation = Quaternion.Slerp(this.transform.rotation, Quaternion.LookRotation(direction), 0.03f);
        this.transform.Translate(0, 0, speed);
        if (Vector3.Distance(player.position, this.transform.position) < 20)
        {
            PerformFiring();
        }
        else
        {
            DeactivateBullets();
        }
    }

    private void OnEnemyDeath()
    {
        isControlledEnabled = false;
        Destroy(gameObject, 2.0f);
    }

    private void PerformFiring()
    {
        bullets.SetActive(true);
    }

    private void DeactivateBullets()
    {
        bullets.SetActive(false);
    }
}
