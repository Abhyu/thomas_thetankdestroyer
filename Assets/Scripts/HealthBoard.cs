﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HealthBoard: MonoBehaviour {

    int health = 100;
    Text healthText;    

	// Use this for initialization
	void Start () {
        healthText = GetComponent<Text>();
        healthText.text = health.ToString();
    }

    public void setHealth(int _health)
    {
        health = _health;
    }
    public void ReduceHealth(int _hits)
    {
        if (health > 0)
        {
            health = health - _hits;
            healthText.text = health.ToString();
        }
    }
}
